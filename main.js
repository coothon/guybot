﻿/* INIT */
// To interact with files
const fs = require(`fs`);

// Your bot token, make a new folder in the same dir as this file and call it `key`
// Then make a file with no extension in that folder called `bottoken` and paste your bot token into it
// If you're on Windows you might have to replace the `/` with `\\` everywhere that fs is used, not sure
const botToken = fs.readFileSync(`./key/bottoken`, {encoding: `utf8`, flag: `r`});

//Required discord init
const Discord = require(`discord.js`);
const { findSourceMap } = require("module");
const client = new Discord.Client({ intents: [`GUILDS`, `GUILD_MESSAGES`, `DIRECT_MESSAGES`, `DIRECT_MESSAGE_REACTIONS`, `DIRECT_MESSAGE_TYPING`]});

// Required at start of bot command
const prefix = `<guy>`;

// Required at end of bot command. Can be removed
const suffix = `</guy>`;

// Here for ease-of-use
const sampleWords = [ `dick`, `yes`, `balls`, `no`];

let userLogFile;

const helpText = 
`**Usage:**
\tFor single commands:
\t\t${prefix}*<command>*${suffix}
\tFor multiple commands:
\t\t${prefix}*<command1>*, *<command2>*, *<command3>*${suffix}

**List of available commands:**
\t*sex*                 The bot agrees with you.
\t*dybn*               Returns a pseudo-random sequence of the words: [${sampleWords}].
\t*help*                Displays this.
\t*megasex*        Displays \"sex\" a random amount of times.
`;
/* END INIT */

/* FUNCTIONS */

/* LOGGING */
function LogMessage(varMessage) {
	// Create log file if it doesn't exist
	try {
		fs.readFileSync(`./UserLogs/${varMessage.author.id}.log`, {encoding: `utf8`, flag: `r`});
	} catch(e) {
		fs.writeFileSync(`./UserLogs/${varMessage.author.id}.log`, `${varMessage.author.tag}\n`, {encoding: `utf8`, flag: `w+`});
	}

	// Append time and message contents to log file
	fs.appendFileSync(`./UserLogs/${varMessage.author.id}.log`, `${GetTimeUTC()} | <|${varMessage.content}|>\n`);
	console.log(`Message logged from ${varMessage.author.tag}`);
}
/* END LOGGING */

function GetTimeUTC() {
	let logTime = new Date();
	let logDate = logTime.getUTCDate();
	let logMonth = logTime.getUTCMonth();
	let logYear = logTime.getUTCFullYear();
	let logHour = logTime.getUTCHours();
	let logMin = logTime.getUTCMinutes();
	let logSec = logTime.getUTCSeconds();
	var formattedTime = `${logDate}/${logMonth}/${logYear} | ${logHour}:${logMin}:${logSec}`;
	return formattedTime;
}

function RandomWord() {
	// Important to be init to all false, will be stuck in loop forever otherwise
	var boolArray = [ false, false, false, false ];
	// Empty string[] for output
	var outputWords = [``, ``, ``, ``];
	var index = 0;
	while(true) {
		var currentNum = Math.floor(Math.random() * 4);
		
		// If sample word not taken
		if(boolArray[currentNum] === false) {
			// Insert it into output array and mark it taken
			outputWords[index] = sampleWords[currentNum];
			boolArray[currentNum] = true;
			// Then iterate to next word slot
			index = index + 1;
			if (index === 4) {
				var formattedOutput = `${outputWords[0]} ${outputWords[1]} ${outputWords[2]} ${outputWords[3]}`;
				return formattedOutput;
			}
		}
	}
}

function RandomStrDuplicator(subject) {
	if (typeof subject === 'string' && Object.prototype.toString.call(subject) === '[object String]' ) {
	var finalStr = `${subject}`;
	var amount = Math.floor(Math.random() * 100);
	while (amount > 0) {
		finalStr = `${finalStr} ${subject}`;
		--amount;
	}
	while (finalStr.length > 1000) {
		finalStr = finalStr.slice(0, finalStr.length - 1); 
	}
	return finalStr;
	}
	console.log(`input not a string`);
	return null;
}

function HandleCommands(message) {

	const channel = client.channels.cache.get(message.channelId);
	const args = message.content.slice(prefix.length, message.content.length - suffix.length).split(`, `);
	
	var command = args.shift().toLowerCase();
	var userID = message.author;
	do {
		var finalMessage = `\n${userID}\n`;
		
/* COMMANDS */
		switch(command)
		{
			case `sex`:
				finalMessage = finalMessage + `uhh guys yea\n`;
				break;
			case `megasex`:
				finalMessage = finalMessage + `${RandomStrDuplicator(`sex`)}`;
				break;
			case `dybn`:
				finalMessage = finalMessage + `${RandomWord()}\n`;
				break;
			case `help`:
				finalMessage = finalMessage + `${helpText}`
				break;
			case ``: // To avoid empty commands being flagged as unknown
				finalMessage = finalMessage + `**No Command provided**\n${helpText}`
				break;
			default:
				finalMessage = finalMessage + `**Unknown command:** *${command}*\n${helpText}`;
				break;
		}
/* END COMMANDS */

		channel.send(finalMessage);
		console.log(`\nSending:\n------${finalMessage}------\n`);

		// Throws an error if next command doesn`t exist, so we can break out of the loop
		try {
			command = args.shift().toLowerCase();
		} catch (e) {
			command = null;
		}
	} while(command !== null)
	console.log(`Message processed`);
}
/* END FUNCTIONS */

/* DISCORD */
client.once(`ready`, () => {console.log(`guybot is up`);});

client.on(`messageCreate`, message => {
	console.log(`Message recieved`);
	if (message.author.bot) return;

	LogMessage(message);

	if(!message.content.startsWith(prefix) || !message.content.endsWith(suffix)) return;

	console.log(`Command recieved`);
	HandleCommands(message);
});

client.options;

// Must stay at the end of the file
client.login(botToken);
/* END DISCORD */